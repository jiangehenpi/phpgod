# PHP技术大全

「PHP技术大全」微信公众号原创文章

## 环境说明

1. 若无特别说明，请使用 PHP7.x 系列版本

## 本书目录

* 1.[Laravel 5.5 技术精要](01.0.md)
  * 1.1. [服务容器 Service Container](01.1.md) :fa-times:
  * 1.2. [服务提供者 Service Provider](01.2.md):fa-times:
  * 1.3. [门面模式 Facade Pattern](01.3.md):fa-times:
  * 1.4. [抽象合约 Contract](01.4.md):fa-times:
  * 1.5. [请求生命周期 Request Lifecycle](01.5.md):fa-times:
  * 1.6. [性能优化 Performance Tuning](01.6.md):fa-times:
  * 1.7. [安全与加密 Security And Encrypt](01.7.md):fa-times:
  * 1.8. [广播 Broadcasting](01.8.md):fa-times:
  * 1.9. [通知 Notifications](01.9.md):fa-times:
  * 1.10. [队列 Queues](01.10.md):fa-times:
  * 1.11. [任务调度 Task Scheduling](01.11.md):fa-times:
  * 1.12. [邮件 Mail](01.12.md):fa-times:

* 2.[《PHP技术大全·第一卷》之你可能不知道的 PHP](02.0.md)
  * 2.1. [isset,empty,is_null 区别与联系](02.1.md)
  * 2.2. [foreach](02.2.md)
  * 2.3. [static 静态修饰符](02.3.md)
  * 2.4. [array 数组](02.4.md)
  * 2.5. [generator 生成器](02.5.md)
  * *2.6. [正则表达式](02.6.md)
  * *2.7. [PHP 引用&](02.7.md)
  * *2.8. [GC 垃圾回收](02.8.md)
  * *2.9. [PHP 命令行](02.9.md)
  * *2.10. [PHP 生命周期](02.10.md)
  * *2.11. [PHP 数值计算](02.11.md)
  * *2.12. [PHP-FPM 如何工作](02.12.md)
  * *2.13. [PHP-SAPI 如何工作](02.13.md)
  * *2.14. [Swoole 优点与缺点](02.14.md)
  * *2.15. [PHP 框架选择](02.15.md)
  * *2.16. [通用文件上传](02.16.md)
  * *2.17. [Excel 高效处理](02.17.md) <https://github.com/viest/php-ext-excel-export>
  * *2.18. [CPU 密集型和 IO 密集型须知](02.18.md)
  * *2.19. [PHP-FPM 为何只适合低并发网站](02.19.md)
  * *2.20. [Redis 和 Memcache 如何选择](02.20.md)
  * *2.21. [乐观锁和悲观锁](02.21.md)
  * *2.22. [Zend Engine 如何工作](02.22.md)
  * *2.23. [PHP 应用持续集成](02.23.md)
  * *2.24. [运算符优先级](02.24.md)
  * *2.25. [HHVM 和ZendVM 区别与联系](02.25.md)
  * *2.26. [PHP 源码加密的原理](02.26.md)
  * *2.27. [PHP 为何不适合使用 pthreads 多线程扩展](02.27.md)
  * *2.28. [PHP NTS 与 TS 版本的区别与联系](02.28.md)
  * *2.29. [注解解析](02.29.md)
  * *2.30. [Ev 与 Eio 扩展](02.30.md)
  * *2.31. [JSON 解析问题](02.31.md)
  * *2.32. [PHP 远程调试](02.32.md)
  * *2.33. [Parsekit 扩展](02.33.md)
  * *2.34. [Quickhash 扩展](02.34.md)
  * *2.35. [runkit 扩展](02.35.md)
  * *2.36. [PHPtrait 和 interface 的作用](02.36.md)
  * *2.37. [OPcache 如何工作](02.37.md)
  * *2.38. [PHP,Go,Node 三种敏捷系语言对比](02.38.md)
  * *2.39. [多字节字符串处理](02.39.md) <http://php.net/manual/en/book.mbstring.php>
  * *2.40. [Docker 更适合 PHP 应用](02.40.md)
  * *2.41. [phptrace 扩展与 strace 命令](02.41.md) <https://github.com/Qihoo360/phptrace>
  * *2.42. [PHP 与 kafka 通信](02.42.md) <https://github.com/weiboad/kafka-php>
  * *2.43. [PHP 机器学习](02.43.md) <https://github.com/php-ai/php-ml>
  * *2.44. [Gender 性别判定扩展](02.44.md)
  * *2.45. [Sodium 加密扩展](02.45.md)
  * *2.46. [output buffer 输出缓冲区解析](02.46.md)
  * *2.47. [调用外部程序](02.47.md)<http://php.net/manual/en/book.exec.php>
  * *2.48. [信号量与共享内存处理](02.48.md)<http://php.net/manual/en/book.sem.php>
  * *2.49. [uopz 操作 Zend 扩展](02.49.md)<https://pecl.php.net/package/uopz>
  * *2.50. [sync 同步扩展](02.50.md)
  * *2.51. [PHP 模板引擎设计的难点](02.51.md)
  * *2.52. [PHP 调用lua脚本](02.52.md) <https://pecl.php.net/package/lua>
  * *2.53. [长连接和短连接的区别与联系](02.53.md)
  * *2.54. [Composer 自建镜像](02.54.md)
  * *2.55. [PHP-CS 工具](02.55.md)
  * *2.56. [PHP-MD 工具](02.56.md)
  * *2.57. [Zepir 开发 PHP 原生扩展](02.57.md)
  * *2.58. [PSR 的意义](02.58.md)
  * *2.59. [PHP 标准库 SPL](02.59.md)
  * *2.60. [Taint 扩展](02.60.md) <https://pecl.php.net/package/taint>
  * *2.61. [PHP JIT 如何工作](02.61.md) <https://github.com/zendtech/php-src/tree/jit-dynasm/ext/opcache/jit>
  * *2.62. [Composer 性能优化](02.62.md)
  * *2.63. [PHP Unit 单元测试](02.63.md)
  * *2.64. [PHPStorm 使用](02.64.md)
  * *2.65. [PHP 性能优化](02.65.md)
  * *2.66. [PHP 邮件发送](02.66.md)

* 3.[PHP面试 100 题](03.0.md)
  * *3.1. [session 和 cookie 的区别与联系？](03.1.md)

* 4.[unix系统编程](04.0.md)
  * *4.1. [fork 函数](04.1.md)

* 9.[PHP 易错知识点](09.0.md)
  * 9.1. [unset 键为0-N连续数字的数组](09.1.md)
  
* 10.[visual studio code 使用技巧](10.0.md)
  * *10.1. [vscode 批量编辑](10.1.md)
