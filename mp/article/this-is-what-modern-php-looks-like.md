# 这才是现代PHP该有的样子
![现代化的PHP](https://gitee.com/uploads/images/2018/0531/103151_84267ca3_1174423.png "屏幕截图.png")

标题真的很自恋，不是吗？是啊，就是。虽然我使用了PHP工作多年，但要我怎样陈述好这项工作的最佳实践和工具呢？却不能做得很好，但我会尽量尝试。

我看到开发人员使用PHP开展工作的方式发生了真正的变化，不仅因为PHP发布了新版本和改进，而且PHP语言发生了巨大变化，变得更加成熟和强大，整个生态系统也在不断变化。

新的工具，库，框架和文章正在被创建，新的模式（套路）正在被定义以使代码更加优雅且易于理解。 有些人正在考虑如何让工作（以及作为开发者的生活）更高效，简洁和有趣。

我不是新趋势的早期采用者，实际上，当我知道一个工具的背后有一个社区支撑的时候，我才会采用这种新工具，也认为它会改善我的工作。我总是尝试按照最佳实践写我的代码。

正因为如此，我开始花时间使用Composer和PHPUnit这些东西。大约一年前，我为所有那些闪亮的新事物敞开了心扉。

首先是PSR，然后是Composer，PHPUnit，Travis-ci和其他一些库和令人惊叹的工具。我现在甚至使用IDE（Vim FTW，但带有XDebug集成的PHPStorm是理想工作流程必须的）！

### 什么叫现代

![现代PHP](https://gitee.com/uploads/images/2018/0531/105803_6e19b46c_1174423.png "屏幕截图.png")

网络上有很多关于PHP有多糟糕的文章，如果你不得不使用PHP代码，你的生活会变得如何，语言如何丑陋，以及你能想到的任何其他东西！

如果你打算使用传统的代码，也许你的体验并不那么好，但如果你有机会开发一个新项目并且能够使用所有新工具，那么你将会看到这个我将要谈的全新PHP。

我在使用PHP进行日常工作的时候总会遇到一点问题，但我们不能忽视PHP语言、社区和生态系统发生的变化。因为前面有很长的路要走，并且PHP的领地已经越来越成熟。

我开始为我工作的公司创建一个内部API的SDK时，就像一个宠物项目，并决定遵循最佳实践。 这些最佳实践的大部分我已经在做，但我在做某些事情方面做了一些新的尝试。 这些变化以及我在去年学到的知识是本文的主题，我称之为Modern PHP（摩登PHP）。

### 让我们开始这个流程
![流程开始](https://gitee.com/uploads/images/2018/0531/110743_a3d59270_1174423.png "屏幕截图.png")

正如我所说，我是PHPStorm这个IDE的新手，但对它是一见钟情。 PHPStorm是一款伟大的软件。它是我第一个也是唯一的IDE。它是我的好基友，我甚至不需要尝试任何其它的IDE。

与XDebug的集成是完美的，PHP名称空间解析，composer集成，git集成，自动完成，代码生成，代码重构。它们让我可以保持继续前进。

我不认为你必须使用IDE，实际上，这一点完全是个人选择。 你应该使用任何适合你的需求 - Vim，Atom，Emacs，Bracket，NetBeans，PHPStorm，Eclipse等等。 这里有两个重点：生产力和人体工程学。 您的IDE /文本编辑器必须在这两点上为您提供帮助。

但是，对我来说，一个重要的点是调试器集成。 要为大型项目编写代码（实际上也适用于小型项目），您必须使用体面的调试器。 让我们忘记那些var_dump和print_r们。 您需要在运行时查看这些变量，分析堆栈跟踪，设置断点。 这些东西是必不可少的，并使开发和重构更简单。

我还不知道这里是否还有有其它选项，XDebug拥有您需要的一切。 有几分钟时间吗？ 如果你还没有集成XDebug，就去花点时间设置XDebug并将其集成到你的IDE或文本编辑器中。 这样我们就可以开始使用正确的工具开始调试你的代码。

我想引起您的注意的另一个工具是GitHub。可以编写另一篇关于Git和GitHub的如何优秀的文章，以及解释为什么您必须开始将代码保存在版本控制系统中。但我想告诉你的是另一个原因。

这里的重点是集成（同集成开发环境中的集成一致）。

有几个与GitHub集成的工具，你应该开始使用它们。 这些工具可以生成指标，运行测试，在持续集成过程中为您运行作业，并在您的工作流程中执行各种操作。 集成是你开始使用GitHub的一个很好的理由，所有其它的功能都是下一刻需要关注的。

### 依赖管理

![依赖管理](https://gitee.com/uploads/images/2018/0531/112549_258fdb76_1174423.png "屏幕截图.png")

在这个现代化的PHP生态系统中另一个重点便是依赖管理，composer就是用来做这个事的。

composer 已经5岁了，但在我看来，几年前有了大量的采用。也许是因为我不是早期的采用者，也可能是因为PHP开发人员不愿意改变。

此工具为Packagist提供了一个前端，这是一个由PHP库、项目和工具组成的PHP包仓库，其源代码存储在Github（或BitBucket等其他位置）中。

我在本文中讨论的所有库，也可能是您的其中一个宠物项目，可以通过简单的方式添加到您的项目中。

```
$ composer require package_vendor/package_name
```
如果您不知道软件包的提供者，则可以搜索软件包以查找并安装正确的软件包。

如果只是完成依赖管理这项工作，Composer将是一个很好的工具，但它还有很多功能。花点时间安装Composer并阅读[这篇文档](https://getcomposer.org/doc/)。

### 命令行界面来得恰到好处

我非常喜欢使用CLI界面快速尝试创意。 对我来说，最伟大的REPL工具之一就是IPython。 它可以帮助您自动完成代码，让您轻松定义函数，轻松访问文档和其他一些令人惊叹的功能。 对我们来说，这个工具是针对Python的，而不是PHP。

在PHP世界里，我们有一种叫做“交互模式”的东西，可以通过终端访问，只需输入

```
$ php -a
Interactive mode enabled
php >
```
此时，若您处于交互模式之下并可以开始测试某些内容。它可以达到效果，但是这个工具太不直观了。我已经尝试了好几次，但由于我知道IPython有多好，所以我无法再继续使用它。

幸运的是，该块上有一个很酷的新CLI（命令行界面），它的名字是Psysh。 Psysh是一个了不起的工具，充满了有趣的功能，可以使用composer全局安装或每个项目单独安装。

对我来说Psysh最好的功能是内联文档。访问一个PHP函数的文档，而不必前往Php.net是一件很棒的事情。缺点是你需要做一些配置才能完全发挥作用。

```
$ apt-get install php7.1-sqlite3
$ mkdir /usr/local/share/psysh
$ wget http://psysh.org/manual/en/php_manual.sqlite -o /usr/local/share/psysh/php_manual.sqlite
```
第一个命令不是必需的，如果你已经安装了Sqlite，你可以跳过这一步。 第二个命令创建目录来存储文档和第三行是用来下载并将文档保存到先前创建的目录中。 请记住，所有这些命令都必须以root身份运行。

接下来你会看到这个
![psysh](https://gitee.com/uploads/images/2018/0531/114208_6bafc8f2_1174423.png "屏幕截图.png")

前往[psysh](http://psysh.org/)，了解更多这个工具的玩法吧。

### 你应该开始测试

这是我每天都在对自己说的口头禅。跟很多人一样，我并不像TDD（测试驱动开发）建议的那样测试我的代码。我现在正在进行测试，并且在过去的半年中一直这样做，而且前方还有很长的路要走。

我决定在处理复杂的传统项目（译者注：比如PHP5.2面向过程写的项目，或者业务变更多次的项目）时学习如何测试。代码非常易错和死板，以至于我们添加一些代码就会破坏一些（原有的）东西。增加新功能？会搞乱一些常规代码！修复一个bug？会创建另一个bug。

这是一个很大的问题，我在[另一篇文章](https://medium.freecodecamp.org/few-thoughts-on-legacy-hell-e229f76529e0)中讨论了这个问题，并让我开始给测试一个机会。

我要呈现的第一个工具就是[PHPUnit](https://phpunit.de/)，正如它的官方说的那样：
> PHPUnit 是一个面向编程人员的PHP测试框架，它是一个基于xUnit架构的单元测试框架的实例。

所以，PHPUnit是一个帮助您为项目创建测试的框架--单元化的测试。它提供了几个函数来测试代码的结果，并根据这些结果生成一个很好的输出。

当开始考虑测试的时候，我阅读资料并与人交流，发现了另一个很棒的工具，它补充了您在这些统一测试中所做的工作，它是Behat，它是一个PHP的BDD（行为驱动开发）框架。

BDD（行为驱动开发）是来自TDD（测试驱动开发）的开发流程。 这些缩略词现在并不重要，重要的是您可以使用更自然的语言来指定您的测试，它是非技术人员都可以理解的语言。

这种语言称为Gherkin （小黄瓜），用于描述正在测试的可预期行为。使用 Gherkin 的测试描述如下所示：
![Gherkin](https://gitee.com/uploads/images/2018/0531/134103_ad1372f6_1174423.png "屏幕截图.png")

在这些命令行的后面是PHP代码，只要在该方法的PhpDoc注释中指定的行和正则表达式之间存在匹配，就会调用该代码。 此代码使用您的SDK，应用程序或Web系统实现这些步骤以及真正的用户将执行的操作。

Behat工作流程非常流畅。 一切正确配置后，您就开始编写测试功能的所有可能场景。 第一次运行Behat时，它会为您提供应添加到PHP Context类中的所有方法模板，以便实现一个场景中的每个步骤。

接着，您开始为每个步骤编写实际代码并继续重复此循环：

- 为步骤实施PHP代码 
- 运行测试 
- 如果一切顺利，请为另一个步骤编写PHP代码 
- 如果有问题，请修复

在配置和阅读文档半小时后，您准备使用Behat，您会发现它实际是所有PHP代码，并且您已经知道如何使用它编程。

### 持续集成
持续集成（CI）是一个流程，一种做事的方式，对于我们软件工程师来说，这件事正在创造软件。

用通俗的话来说，这就是将小块代码不断（可能一天几次）加入代码库的行为。 代码已经过测试，并没有破坏任何东西（已有功能）。 CI可帮助您自动化应用程序的构建，测试和部署。

只需点击几下，您就可以将您的GitHub项目与Travis CI集成在一起，并且每次推送到您的存储库都将运行您使用PHPUnit和Behat创建的测试，告诉您实现的最后一个功能是否已准备好，是否合并。 除此之外，您可以使用Travis CI将您的代码部署到生产和预发布环境。

通过一个明确定义的流程来完成工作流程非常好，Travis CI可以帮助您完成这项工作。 遵循这个不错的入门指南，思考并发现软件开发过程的有趣之处，而不仅仅是代码本身。

### 坚守PSR-1和PSR-2

如果你还不知道PSR是什么，但你需要知道。 实际上，PSR代表PHP标准建议，由PHP-FIG（PHP框架Interop组织）提出，该组织由来自最大的PHP项目，框架和CMS的成员组成，该组织正在考虑PHP语言本身，生态系统和讨论的未来应遵循的标准。

很长一段时间，PHP没有固定的编码风格。 虽然我还没那么老，但每次看到某人的项目或库时，都会遵循不同的风格。 有时候，括号留在一个位置，有时放在下一行，不同的方法用于处理长代码行和其他风格及偏好的你可以想象得到的组合。 真是一团糟啊。

PHP-FIG做了很多其他的工作，但是通过提出一个统一的代码，他们说“让我们不再担心代码风格，让每个人都遵循标准，并开始考虑创建优秀的软件”。 现在，每当你看一个人的代码时，你只需要担心它是如何工作的，而不是指责格式和结构。

直到本文结束时，有9个被接受的PSR提出常见问题的共同解决方案。但如果您对这些标准一无所知，请从[PSR-1](http://www.php-fig.org/psr/psr-1/)和[PSR-2](http://www.php-fig.org/psr/psr-2/)开始。

这些标准提出了现代PHP编码风格。确保在开始使用它们之前阅读它们。不要以为在编码时你会记得所有这些标准，它是一个流程，但为了让你明确，有一些相关工具可以帮助你。

PHP CodeSniffer是一个可以在Packagist上找到的工具，您可以使用Composer进行安装。 **我不认为存储库名称是最好的选择，因为它提供了两个不同的工具** ，phpcs和phpcbf。

Phpcs是代码嗅探器，它会扫描你的整个代码，寻找不符合配置编码标准的部分。

你可以在phpcs中配置多种编码标准，甚至可以创建自己的编码标准。在代码扫描结束时，phpcs会向您显示不符合标准的代码段列表。这功能太好了。

现在，如何改变一切错误的东西呢？ 您可以打开每个文件，更改代码，再次运行phpcs，查看未显示的错误，然后重复该流程。 这将是非常无聊的一件事。

为了解决这个问题，PHP CodeSniffer提供了另一个名为phpcbf或PHP Code Beautifier的工具。您运行phpcbf，遵循相同的规则集，并且，它会为您修复所有内容，或者尝试在不破坏代码功能的情况下尽力而为。

尝试创建运行phpcs和phpcbf的习惯，然后将代码中的任何更改推送到存储库，这样可以确保您的所有代码都符合标准，并且如果有人喜欢您的工具/项目并想贡献它们，他们将不会在阅读代码的时候出现疑问（因为代码风格问题）。

# 框架

我不会花太多时间讨论框架。 有几个很好的，每个都有它的优缺点。 就个人而言，我不想使用那些大而全框架，以及框架里面的一切。 我喜欢这个想法--你必须使用你所需要的。

如果您需要HTTP客户端，请使用Guzzle。如果您需要模板引擎，请使用Twig。如果你需要路由器，找一个适合你的需求并使用它的好组件。将这些组件粘合在一起并创建您的应用程序。

[Symfony](https://symfony.com/)在这个概念上做得很好。您可以为整个项目使用整个框架，或者您可以随心所欲地使用它。就那么简单。

但是，无论何时我需要一个框架来编写应用程序，我都会选择一个所谓的微框架。他们非常小，只提供基础元素（例如路由中间件），易于定制，并且更容易使他们跟随您的项目结构自主变化。

我选择的微框架是[Slimframework](https://www.slimframework.com/)，我认为你应该阅读它。做小型项目很简单，但对于较大型项目而言，它会变得更为复杂。

顺便说一句，对于那些刚开始编程的人来说，我真的认为在采用框架并为之痴迷之前，你应该尝试创建自己的框架。 这会让你对整个机制有所了解，并可以减少采用更大的框架。

### 现代的PHP工具集

让我们用一个链接列表来结束这篇文章。对我而言，这些组件，工具和库代表了Modern PHP的很多功能：

- Slimframework: 一个漂亮和酷酷的微框架
- Symfony: 一个拥有强大可重用组件的较大点的框架
- Guzzle: 一个简单易用的 HTTP 客户端
- PHPUnit: 一个用于单元测试的框架
- Behat: 一个行为驱动开发的框架
- PHPCS/CBF: 代码风格嗅探和代码美化工具
- Faker: 伪数据生成器
- Psysh: 一个充满令人惊叹功能的运行时开发者命令行工具
- Composer: 依赖管理以及其它一些好用的功能
- Packagist: 包仓库
- Twig: 模板引擎

我知道，标题真的很自恋。我真正想在这里展示的是，PHP正在发展，它的生态系统正在以同样的速度发展（可能更快）。

戳[这里](https://medium.freecodecamp.org/this-is-what-modern-php-looks-like-769192a1320)看英文原文。
